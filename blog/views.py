from django.shortcuts import render
from django.views import generic

from blog.models import Post, Comment
from blog.common.utils import LoginRequiredMixin

class IndexView(generic.ListView):
    model = Post
    template_name = 'blog/index.html'
    context_object_name = 'latest_post_list'
    
    def get_queryset(self):
        """return the 4 most recent posts"""
        return Post.objects.order_by('-published')[:4]
    
    
class PostView(LoginRequiredMixin, generic.DetailView):
    model = Post
    template_name = 'blog/detail.html'

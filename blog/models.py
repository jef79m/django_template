from django.db import models

class Post(models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField()
    published = models.DateTimeField('date_published')
    
    def __unicode__(self):
        return self.title
    
class Comment(models.Model):
    post = models.ForeignKey(Post)
    comment = models.CharField(max_length=200)
    author = models.CharField(max_length=50)
    
    def __unicode__(self):
        return(''.join((self.comment[:25],'...')))
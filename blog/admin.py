from django.contrib import admin
from blog.models import Post, Comment

class CommentInline(admin.StackedInline):
    model = Comment
    extra = 1
    
class PostAdmin(admin.ModelAdmin):
    inlines = [CommentInline]

admin.site.register(Post, PostAdmin)
from django.conf.urls import patterns, include, url

from django.contrib import admin
from class_based_auth_views.views import LoginView, LogoutView
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'django_test.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^blog/', include('blog.urls')),
    url(r'^login/$', LoginView.as_view(redirect_field_name=next), name="login"),
    url(r'^logout/$', LogoutView.as_view(), name="logout"),
)
